#!/bin/bash 
echo "running karma..."
#RAILS_ENV=test bundle exec rake spec:javascript > consoleOutput/jasmine_output.txt

karma start karma.conf.js > consoleOutput/jasmine_output.txt

grep -q 'FAILED' consoleOutput/jasmine_output.txt
failed=$?
grep -q 'ERROR' consoleOutput/jasmine_output.txt
error=$?

ans=$((failed * error))

if (($ans == 0))
	then 
	echo 'jasmine 	[ Fail ]'
	exit 0
else
	echo 'jasmine 	[ Pass ]'
	exit 1
fi