#!/bin/bash 
echo "running Git..."
find . -name '*.orig' >> consoleOutput/git_output.txt
find . -name '*.LOCAL' >> consoleOutput/git_output.txt
find . -name '*.REMOTE' >> consoleOutput/git_output.txt
find . -name '*.BACKUP' >> consoleOutput/git_output.txt
find . -name '*.BASE' >> consoleOutput/git_output.txt
grep -c -q '.' consoleOutput/git_output.txt
if (($? == 0))
then 
	echo 'Git             [ Fail ]'
	exit 0
else
	echo 'Git             [ Pass ]'
	exit 1
fi