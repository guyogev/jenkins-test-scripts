#!/bin/bash 
echo "running Ruby_syntax..."
find . -name '*.rb' -exec ruby -c  {} \; &> consoleOutput/ruby_syntax_output.txt
grep 'syntax error' consoleOutput/ruby_syntax_output.txt
if (($? == 0))
then 
	echo 'ruby syntax 	[ Fail ]'
	exit 0
else
	echo 'ruby syntax 	[ Pass ]'
	exit 1
fi