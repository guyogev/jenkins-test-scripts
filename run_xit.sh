#!/bin/bash 
echo "running xit..."
find . -wholename "./spec*.*" -exec grep -H 'xit '  {} \;  > consoleOutput/xit_output.txt
grep -q -L 'xit' consoleOutput/xit_output.txt
if (($? == 0))
then 
	echo 'xit 		[ Fail ]'
	exit 0
else
	echo 'xit	 	[ Pass ]'
	exit 1
fi