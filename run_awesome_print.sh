#!/bin/bash 
echo "running awesome_print..."
find . -name '*.rb' -exec grep -H 'awesome_print'  {} \; > consoleOutput/awesome_print_output.txt
grep -q -L 'awesome_print' consoleOutput/awesome_print_output.txt
if (($? == 0))
then 
	echo 'awesome print 	[ Fail ]'
	exit 0
else
	echo 'awesome print 	[ Pass ]'
	exit 1
fi