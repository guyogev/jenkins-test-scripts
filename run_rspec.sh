#!/bin/bash 
echo "running Rspec..."
COVERAGE=true rspec > consoleOutput/rspec_output.txt
grep -q '0 failures' consoleOutput/rspec_output.txt
if (($? > 0))
	then 
	echo 'rspec 		[ Fail ]'
	exit 0
else
	echo 'rspec 		[ Pass ]'
	exit 1
fi