#!/bin/bash 
echo "running jslint..."
find . -wholename "./app/assets/javascripts/*.js" -exec jslint --nomen --plusplus --newcap --vars --indent=2 --white {} \;  >> consoleOutput/jslint_output.txt
find . -wholename "./spec/javascripts/*.js" -exec jslint --nomen --plusplus --newcap --vars --indent=2 --white {} \;  >> consoleOutput/jslint_output.txt
grep -L -q '#' consoleOutput/jslint_output.txt
if (($? == 0))
then 
	echo 'jslint 		[ Fail ]'
	exit 0
else
	echo 'jslint 		[ Pass ]'
	exit 1
fi
