#!/bin/bash 

#clear old tests results
rm -r consoleOutput
mkdir consoleOutput

#if jenkins, set p to match jenkins hierarchy. other users can set $p to their system.
echo `pwd`
whoami | grep 'jenkins'
if (($? == 0))
then
	p='../../jenkins_test_scripts/workspace/'
else 
	p="../../jenkins_test_scripts/"
fi

status=1
$p"run_rspec.sh"
status=$((status * $?))
$p"run_jasmine.sh"
status=$((status * $?))
$p"run_ruby_syntax.sh"
status=$((status * $?))
$p"run_xit.sh"
status=$((status * $?))
$p"run_awesome_print.sh"
status=$((status * $?))
$p"run_jslint.sh"
status=$((status * $?))
$p"run_git.sh"
status=$((status * $?))

exit $status